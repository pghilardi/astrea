package br.com.aurum.astrea.utils;

public class NotValidException extends Exception {

    public NotValidException(String message) {
        super(message);
    }

}
