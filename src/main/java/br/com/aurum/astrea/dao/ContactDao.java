package br.com.aurum.astrea.dao;

import java.util.List;

import br.com.aurum.astrea.domain.Contact;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

public class ContactDao {

	static {
		ObjectifyService.register(Contact.class);
	}

	public void save(Contact contact) {
		ObjectifyService.ofy().save().entity(contact).now();
	}

	public Contact get(Long contactId){
		Key<Contact> key = Key.create(Contact.class, contactId);
		return ObjectifyService.ofy().load().key(key).now();
	}

	public List<Contact> list() {
		return ObjectifyService.ofy().load().type(Contact.class).list();
	}

	public List<Contact> filter(ContactQuery query) {

		Query<Contact> queryResult = ObjectifyService.ofy().load().type(Contact.class);

		// TODO: Investigate how to use multiple inequality filters with Objectify to implement a search like
		// starts with that works
		if (query.cpf != null){
			queryResult = queryResult.filter("cpf =", query.cpf);
		}

		if (query.name != null){
			queryResult = queryResult.filter("name =", query.name);
		}

		if (query.email != null){
			queryResult = queryResult.filter("emails =", query.email);
		}



		return queryResult.list();
	}

	public void delete(Long contactId) {
		Key<Contact> key = Key.create(Contact.class, contactId);
		ObjectifyService.ofy().delete().key(key).now();
	}

}
