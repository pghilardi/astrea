package br.com.aurum.astrea.dao;

public class ContactQuery {

    public String name;
    public String cpf;
    public String email;

    public static class ContactQueryBuilder {

        private String name;
        private String cpf;
        private String email;

        public ContactQueryBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ContactQueryBuilder email(String email) {
            this.email = email;
            return this;
        }

        public ContactQueryBuilder cpf(String cpf) {
            this.cpf = cpf;
            return this;
        }

        public ContactQuery build() {
            return new ContactQuery(this);
        }

    }

    private ContactQuery(ContactQueryBuilder b) {
        this.name = b.name;
        this.cpf = b.cpf;
        this.email = b.email;
    }
}
