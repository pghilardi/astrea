package br.com.aurum.astrea.service;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.aurum.astrea.dao.ContactDao;
import br.com.aurum.astrea.dao.ContactQuery;
import br.com.aurum.astrea.domain.Contact;
import br.com.aurum.astrea.domain.ContactValidator;
import br.com.aurum.astrea.utils.NotValidException;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class ContactServlet extends HttpServlet {
	
	private static final ContactDao DAO = new ContactDao();

	private final String JSON_CONTENT_TYPE = "application/json";

	private final String UTF_8_ENCODING = "utf-8";

	private final String PATH_SEPARATOR = "/";

	private boolean validateSpecificResourcePath(String[] splittedPath, HttpServletResponse resp){
		if (splittedPath.length != 2){
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return false;
		}
		return true;
	}

	private boolean validateResourcePath(String[] splittedPath, HttpServletResponse resp){
		if (splittedPath.length > 1){
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return false;
		}
		return true;
	}

	private boolean validateContact(Contact contact, HttpServletResponse resp) throws IOException {
		ContactValidator validator = new ContactValidator();
		try {
			validator.validate(contact);
			return true;
		} catch (NotValidException e) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			resp.setCharacterEncoding(UTF_8_ENCODING);
			resp.getWriter().write(e.getMessage());
			return false;
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		String[] path = req.getPathInfo().split(PATH_SEPARATOR);
		boolean isValid = validateResourcePath(path, resp);
		if (!isValid){
			return;
		}

		Contact contact = new Gson().fromJson(req.getReader(), Contact.class);
		if (!validateContact(contact, resp)) {
			return;
		}

		if (contact.getCpf() != null){
			ContactQuery query = new ContactQuery.ContactQueryBuilder().cpf(contact.getCpf()).build();
			if (DAO.filter(query).size() > 0){
				resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				resp.getWriter().write("Already exists a contact with this CPF");
				return;
			}
		}

		DAO.save(contact);
		resp.setStatus(HttpServletResponse.SC_CREATED);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String[] path = req.getPathInfo().split(PATH_SEPARATOR);
		boolean isValid = validateSpecificResourcePath(path, resp);
		if (!isValid){
			return;
		}

		Long contactId = Long.valueOf(path[path.length - 1]);
		Contact currentContact = DAO.get(contactId);
		if (currentContact == null){
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		Contact contact = new Gson().fromJson(req.getReader(), Contact.class);
		if (!contactId.equals(currentContact.getId())){
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			resp.getWriter().write("The contact id on JSON must be the same of the URL id");
			return;
		}

		if (contact.getCpf() != null) {
			if (!contact.getCpf().equals(currentContact.getCpf())) {
				// CPF has changed, check that not clashes with other CPFs
				ContactQuery query = new ContactQuery.ContactQueryBuilder().cpf(contact.getCpf()).build();
				if (DAO.filter(query).size() > 0) {
					resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					resp.getWriter().write("Already exists a contact with this CPF");
					return;
				}
			}
		}

		if (!validateContact(contact, resp)){
			return;
		}

		DAO.save(contact);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		String contactsAsJson;
		if (req.getPathInfo() == null){

			String cpf = req.getParameter("cpf");
			String name = req.getParameter("name");
			String email = req.getParameter("email");
			ContactQuery.ContactQueryBuilder builder = new ContactQuery.ContactQueryBuilder();
			if (cpf != null) {
				builder = builder.cpf(cpf);
			}

			if (name != null) {
				builder = builder.name(name);
			}

			if (email != null){
				builder = builder.email(email);
			}

			contactsAsJson = new Gson().toJson(DAO.filter(builder.build()));
		} else {

			String[] path = req.getPathInfo().split(PATH_SEPARATOR);
			if (path.length == 2){
				// Handle resource get/:id
				Long contactId = Long.valueOf(path[path.length - 1]);
				contactsAsJson = new Gson().toJson(DAO.get(contactId));
			} else if (path.length > 1){
				// Invalid URL
				resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
				return;
			} else {
				// Handle resource list/
				contactsAsJson = new Gson().toJson(DAO.list());
			}
		}

		resp.setContentType(JSON_CONTENT_TYPE);
		resp.setCharacterEncoding(UTF_8_ENCODING);
		resp.getWriter().write(contactsAsJson);
	}
	
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String[] path = req.getPathInfo().split(PATH_SEPARATOR);
		if (!validateSpecificResourcePath(path, resp)){
			return;
		}

		Long contactId = Long.valueOf(path[path.length - 1]);
		if (DAO.get(contactId) == null){
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		DAO.delete(contactId);
	}
}
