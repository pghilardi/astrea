package br.com.aurum.astrea.domain;

import br.com.aurum.astrea.utils.NotValidException;

import java.util.ArrayList;
import java.util.List;

public class ContactValidator {

    public void validate(Contact contact) throws NotValidException {

        // TODO: Here we can validate against valid CPFs in future and other required fields

        List<String> problemsFound = new ArrayList<>();

        if (contact.getName() == null){
            problemsFound.add("The name cannot be empty");
        }

        String message = "";
        for (String problem : problemsFound){
            message += problem + "\n";
        }

        if (problemsFound.size() > 0 ){
            throw new NotValidException(message);
        }

    }

}
