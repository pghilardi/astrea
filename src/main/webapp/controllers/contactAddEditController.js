var contactAddEditController;

contactAddEditController = function($scope, $http, $state, $stateParams) {

	var contactId = $stateParams.contactId;
	if (contactId != null){
        $http.get('/contacts/' + contactId).then(function(response){
        	console.log('contact loaded with success');
			$scope.contact = response.data;
        }, function(response){
            console.log('problem loading contact');
		})
    } else {
        $scope.contact = {};
        $scope.contact.emails = [''];
        $scope.contact.phones = [''];
	}

	$scope.submitted = false;
	
	$scope.save = function() {

		$scope.submitted = true;
		if ($scope.contact.name != null && $scope.contact.name != "") {

			if ($scope.contact.id!= null){

                $http.put('/contacts/' + $scope.contact.id, $scope.contact).then(function(response){
                    console.log('success updating contact');
                    $state.go('main.contacts');
                }, function(response){
                    console.log('error updating contact');
                });
            } else {
                $http.post('/contacts/', $scope.contact).then(function(response){
                    console.log('success creating contact');
                    $state.go('main.contacts');
                }, function(response){
                    console.log('error creating contact');
                });
            }

        }

	};

	$scope.addMorePhones = function() {
		$scope.contact.phones.push('');
	}; 

	$scope.addMoreEmails = function() {
		$scope.contact.emails.push('');
	};

	$scope.deletePhone = function(index){
		if (index > -1) {
    		$scope.contact.phones.splice(index, 1);
		}

		if ($scope.contact.phones.length < 1){
			$scope.addMorePhones();
		}
	};

	$scope.deleteEmail = function(index){
		if (index > -1) {
    		$scope.contact.emails.splice(index, 1);
		}

		if ($scope.contact.emails.length < 1){
			$scope.addMoreEmails();
		}
	};

};

angular.module('avaliacandidatos').controller("contactAddEditController", contactAddEditController);