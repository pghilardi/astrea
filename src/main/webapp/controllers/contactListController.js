var contactListController;

contactListController = function($scope, $http, $state) {
	$scope.contacts = [];
	$scope.preDeletedContact = {};

	$scope.searchName = '';
	$scope.searchCpf = '';
	$scope.searchEmail = '';

	$scope.init = function() {
		$scope.listAllContacts();
	};
	
	$scope.listAllContacts = function() {

		$http.get('/contacts').then(function(response) {
			$scope.contacts = response.data;
		}, function(response) {
            console.log('error retrieving contacts');
		});
	};

	$scope.preDelete = function(contact) {
		$scope.preDeletedContact = contact;
		$('#myModal').modal('show');
	};

	$scope.delete = function() {

		if($scope.preDeletedContact != null) {
			$http.delete('/contacts/' + $scope.preDeletedContact.id).then(function(response){
				console.log('contact deleted with success');
                $scope.listAllContacts();
                $('#myModal').modal('hide');
            }, function(response) {
				console.log('error deleting contacts');
			})
		}
	};

	$scope.clearSearch = function() {
		$scope.searchCpf = "";
		$scope.searchEmail = "";
		$scope.searchName = "";
	};

    $scope.searchEntities = function() {

    	if ($scope.searchName != '' || $scope.searchCpf != '' || $scope.searchEmail != ''){

    		var url = '/contacts?';
    		if ($scope.searchName != ''){
    			url = url + 'name=' + $scope.searchName + '&';
            }
            if ($scope.searchEmail != ''){
    			url = url + 'email=' + $scope.searchEmail+ '&';
            }
            if ($scope.searchCpf != ''){
    			url = url + 'cpf=' + $scope.searchCpf+ '&';
            }

    		$http.get(url).then(function(response){
				$scope.contacts = response.data;
			}, function(response){
				console.log('error searching contacts');
			})

		} else {
    		$scope.listAllContacts();
		}
    };

    $scope.edit = function(c) {
        $state.go('main.edit', {'contactId' : c.id});
    };

	$scope.bday = function(c) {
		if(c.birthDay==null || c.birthDay == ""){
			return "";
		} else {
			return c.birthDay + "/" + c.birthMonth + "/" + c.birthYear;
		}
	};
};

angular.module('avaliacandidatos').controller("contactListController", contactListController);