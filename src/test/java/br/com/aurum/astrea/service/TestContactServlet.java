package br.com.aurum.astrea.service;

import br.com.aurum.astrea.domain.Contact;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.testng.annotations.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class TestContactServlet {

    private final LocalServiceTestHelper helper = new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
    private ContactServlet servlet;

    @BeforeMethod
    public void setUp() {
        ObjectifyService.setFactory(new ObjectifyFactory());
        ObjectifyService.register(Contact.class);

        servlet = new ContactServlet();
        helper.setUp();
    }

    @AfterMethod
    public void tearDown() {
        ObjectifyService.reset();
        helper.tearDown();
    }

    @Test
    public void testCreateContactWithSuccess() throws IOException {
        HttpServletRequest  mockedRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse  mockedResponse = Mockito.mock(HttpServletResponse.class);

        String createPath = "contacts/";
        Mockito.when(mockedRequest.getPathInfo()).thenReturn(createPath);

        Contact contact = new Contact();
        contact.setName("Pedro");
        contact.setCpf("88800012345");

        String c = new Gson().toJson(contact);
        InputStream is = new ByteArrayInputStream(c.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        Mockito.when(mockedRequest.getReader()).thenReturn(br);

        PrintWriter mockedWriter = Mockito.mock(PrintWriter.class);
        Mockito.when(mockedResponse.getWriter()).thenReturn(mockedWriter);

        servlet.doPost(mockedRequest, mockedResponse);

        List<Contact> contacts = ObjectifyService.ofy().load().type(Contact.class).list();
        assertEquals(1, contacts.size());
        assertEquals(contact.getName(), contacts.get(0).getName());
        assertEquals(contact.getCpf(), contacts.get(0).getCpf());
    }

    @Test
    public void testCreateContactWithoutRequiredFields() throws IOException {
        HttpServletRequest  mockedRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse  mockedResponse = Mockito.mock(HttpServletResponse.class);

        String createPath = "contacts/";
        Mockito.when(mockedRequest.getPathInfo()).thenReturn(createPath);

        Contact contact = new Contact();

        String c = new Gson().toJson(contact);
        InputStream is = new ByteArrayInputStream(c.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        Mockito.when(mockedRequest.getReader()).thenReturn(br);

        PrintWriter mockedWriter = Mockito.mock(PrintWriter.class);
        Mockito.when(mockedResponse.getWriter()).thenReturn(mockedWriter);

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> captorStatus = ArgumentCaptor.forClass(Integer.class);

        servlet.doPost(mockedRequest, mockedResponse);

        Mockito.verify(mockedResponse, Mockito.times(1)).setStatus(captorStatus.capture());
        Mockito.verify(mockedWriter, Mockito.times(1)).write(captor.capture());

        String message = captor.getValue();
        assertEquals("The name cannot be empty\n", message);
        assertEquals(HttpServletResponse.SC_BAD_REQUEST, captorStatus.getValue().intValue());
    }

    @Test
    public void testCreateContactWithTheSameCPFIsNotPossible() throws IOException{
        HttpServletRequest  mockedRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse  mockedResponse = Mockito.mock(HttpServletResponse.class);

        String createPath = "contacts/";
        Mockito.when(mockedRequest.getPathInfo()).thenReturn(createPath);

        Contact contact = new Contact();
        contact.setName("Abreu");
        contact.setCpf("00011100011");
        ObjectifyService.ofy().save().entity(contact).now();

        Contact otherContact = new Contact();
        otherContact.setName("Josemar");
        otherContact.setCpf("00011100011");

        String c = new Gson().toJson(otherContact);
        InputStream is = new ByteArrayInputStream(c.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        Mockito.when(mockedRequest.getReader()).thenReturn(br);

        PrintWriter mockedWriter = Mockito.mock(PrintWriter.class);
        Mockito.when(mockedResponse.getWriter()).thenReturn(mockedWriter);

        ArgumentCaptor<Integer> captorStatus = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        servlet.doPost(mockedRequest, mockedResponse);

        Mockito.verify(mockedWriter, Mockito.times(1)).write(captor.capture());
        Mockito.verify(mockedResponse, Mockito.times(1)).setStatus(captorStatus.capture());

        assertEquals("Already exists a contact with this CPF", captor.getValue());
        assertEquals(HttpServletResponse.SC_BAD_REQUEST, captorStatus.getValue().intValue());
    }

    @Test
    public void testCreateContactInvalidURL() throws IOException{
        HttpServletRequest  mockedRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse  mockedResponse = Mockito.mock(HttpServletResponse.class);

        String createPath = "contacts/invalid";
        Mockito.when(mockedRequest.getPathInfo()).thenReturn(createPath);

        Contact contact = new Contact();
        contact.setName("Name");
        contact.setCpf("00011100011");

        String c = new Gson().toJson(contact);
        InputStream is = new ByteArrayInputStream(c.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        Mockito.when(mockedRequest.getReader()).thenReturn(br);

        ArgumentCaptor<Integer> captor = ArgumentCaptor.forClass(Integer.class);

        servlet.doPost(mockedRequest, mockedResponse);

        Mockito.verify(mockedResponse, Mockito.times(1)).setStatus(captor.capture());
        int status = captor.getValue();

        assertEquals(HttpServletResponse.SC_NOT_FOUND, status);
    }

    @Test
    public void testUpdateContactInvalidURL() throws IOException{
        HttpServletRequest  mockedRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse  mockedResponse = Mockito.mock(HttpServletResponse.class);

        String createPath = "contacts/";
        Mockito.when(mockedRequest.getPathInfo()).thenReturn(createPath);

        Contact contact = new Contact();
        contact.setName("Bei");
        contact.setCpf("00011100011");

        String c = new Gson().toJson(contact);
        InputStream is = new ByteArrayInputStream(c.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        Mockito.when(mockedRequest.getReader()).thenReturn(br);

        ArgumentCaptor<Integer> captor = ArgumentCaptor.forClass(Integer.class);

        servlet.doPut(mockedRequest, mockedResponse);

        Mockito.verify(mockedResponse, Mockito.times(1)).setStatus(captor.capture());
        int status = captor.getValue();

        assertEquals(HttpServletResponse.SC_NOT_FOUND, status);
    }

    @Test
    public void testUpdateContactWithSuccess() throws IOException{
        HttpServletRequest  mockedRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse  mockedResponse = Mockito.mock(HttpServletResponse.class);

        Contact contact = new Contact();
        contact.setName("Oi");
        contact.setCpf("00011100011");
        Key<Contact> savedContact = ObjectifyService.ofy().save().entity(contact).now();

        String createPath = "contacts/" + savedContact.getId();
        Mockito.when(mockedRequest.getPathInfo()).thenReturn(createPath);

        contact.setName("Tchau");
        contact.setCpf("11111100011");

        String c = new Gson().toJson(contact);
        InputStream is = new ByteArrayInputStream(c.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        Mockito.when(mockedRequest.getReader()).thenReturn(br);

        servlet.doPut(mockedRequest, mockedResponse);

        Key<Contact> key = Key.create(Contact.class, savedContact.getId());
        Contact loadedContact = ObjectifyService.ofy().load().key(key).now();

        assertEquals("Tchau", loadedContact.getName());
        assertEquals("11111100011", loadedContact.getCpf());
    }

    @Test
    public void testGetContactWithSuccess() throws IOException{
        HttpServletRequest  mockedRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse  mockedResponse = Mockito.mock(HttpServletResponse.class);

        Contact contact = new Contact();
        contact.setName("Oi");
        contact.setCpf("00011100011");
        Key<Contact> savedContact = ObjectifyService.ofy().save().entity(contact).now();

        String createPath = "contacts/" + savedContact.getId();
        Mockito.when(mockedRequest.getPathInfo()).thenReturn(createPath);

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        PrintWriter mockedWriter = Mockito.mock(PrintWriter.class);
        Mockito.when(mockedResponse.getWriter()).thenReturn(mockedWriter);

        servlet.doGet(mockedRequest, mockedResponse);

        Mockito.verify(mockedWriter, Mockito.times(1)).write(captor.capture());
        String loadedContact = captor.getValue();

        Contact loadedObject = new Gson().fromJson(loadedContact , Contact.class);
        assertEquals(loadedObject.getName(), contact.getName());
        assertEquals(loadedObject.getCpf(), contact.getCpf());
    }

    @Test
    public void testDeleteContactWithSuccess() throws IOException{
        HttpServletRequest  mockedRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse  mockedResponse = Mockito.mock(HttpServletResponse.class);

        Contact contact = new Contact();
        contact.setName("Oi");
        contact.setCpf("00011100011");
        Key<Contact> savedContact = ObjectifyService.ofy().save().entity(contact).now();

        String createPath = "contacts/" + savedContact.getId();
        Mockito.when(mockedRequest.getPathInfo()).thenReturn(createPath);

        String c = new Gson().toJson(contact);
        InputStream is = new ByteArrayInputStream(c.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        Mockito.when(mockedRequest.getReader()).thenReturn(br);

        servlet.doDelete(mockedRequest, mockedResponse);

        Key<Contact> key = Key.create(Contact.class, savedContact.getId());
        Contact loadedContact = ObjectifyService.ofy().load().key(key).now();

        assertEquals(null, loadedContact);
    }

    @Test
    public void testDeleteContactNotFound() throws IOException{
        HttpServletRequest  mockedRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse  mockedResponse = Mockito.mock(HttpServletResponse.class);

        String createPath = "contacts/" + 123456;
        Mockito.when(mockedRequest.getPathInfo()).thenReturn(createPath);

        ArgumentCaptor<Integer> captor = ArgumentCaptor.forClass(Integer.class);

        servlet.doDelete(mockedRequest, mockedResponse);

        Mockito.verify(mockedResponse, Mockito.times(1)).setStatus(captor.capture());
        int status = captor.getValue();

        assertEquals(HttpServletResponse.SC_NOT_FOUND, status);
    }

    @Test
    public void testListAllContacts() throws IOException {
        HttpServletRequest  mockedRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse  mockedResponse = Mockito.mock(HttpServletResponse.class);

        Contact contact = new Contact();
        contact.setName("Um");
        contact.setCpf("1");
        ObjectifyService.ofy().save().entity(contact).now();

        contact = new Contact();
        contact.setName("Dois");
        contact.setCpf("2");
        ObjectifyService.ofy().save().entity(contact).now();

        contact = new Contact();
        contact.setName("Tres");
        contact.setCpf("3");
        ObjectifyService.ofy().save().entity(contact).now();

        String createPath = "contacts/";
        Mockito.when(mockedRequest.getPathInfo()).thenReturn(createPath);

        PrintWriter mockedWriter = Mockito.mock(PrintWriter.class);
        Mockito.when(mockedResponse.getWriter()).thenReturn(mockedWriter);

        String c = new Gson().toJson(contact);
        InputStream is = new ByteArrayInputStream(c.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        Mockito.when(mockedRequest.getReader()).thenReturn(br);

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        servlet.doGet(mockedRequest, mockedResponse);

        Mockito.verify(mockedWriter, Mockito.times(1)).write(captor.capture());
        String contacts = captor.getValue();

        Type listType = new TypeToken<ArrayList<Contact>>(){}.getType();
        ArrayList<Contact> loadedContacts = new Gson().fromJson(contacts, listType);

        assertEquals(3, loadedContacts.size());
    }


    @Test
    public void testListAllContactsFiltering() throws IOException {
        HttpServletRequest  mockedRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse  mockedResponse = Mockito.mock(HttpServletResponse.class);

        Contact contact = new Contact();
        contact.setName("Um");
        List<String> emails = new ArrayList<>(Arrays.asList("um@aurum.com"));
        contact.setEmails(emails);
        contact.setCpf("1");
        ObjectifyService.ofy().save().entity(contact).now();

        contact = new Contact();
        contact.setName("Dois");
        emails = new ArrayList<>(Arrays.asList("dois@aurum.com", "um@aurum.com"));
        contact.setEmails(emails);
        contact.setCpf("2");
        ObjectifyService.ofy().save().entity(contact).now();

        contact = new Contact();
        contact.setName("Dois");
        contact.setCpf("22");
        ObjectifyService.ofy().save().entity(contact).now();

        contact = new Contact();
        contact.setName("Tres");
        contact.setCpf("3");
        ObjectifyService.ofy().save().entity(contact).now();

        contact = new Contact();
        contact.setName("Tres");
        contact.setCpf("33");
        ObjectifyService.ofy().save().entity(contact).now();

        contact = new Contact();
        contact.setName("Tres");
        contact.setCpf("333");
        ObjectifyService.ofy().save().entity(contact).now();

        Mockito.when(mockedRequest.getParameter("name")).thenReturn("Dois");

        PrintWriter mockedWriter = Mockito.mock(PrintWriter.class);
        Mockito.when(mockedResponse.getWriter()).thenReturn(mockedWriter);

        String c = new Gson().toJson(contact);
        InputStream is = new ByteArrayInputStream(c.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        Mockito.when(mockedRequest.getReader()).thenReturn(br);

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        servlet.doGet(mockedRequest, mockedResponse);

        Mockito.verify(mockedWriter, Mockito.times(1)).write(captor.capture());
        String contacts = captor.getValue();

        Type listType = new TypeToken<ArrayList<Contact>>(){}.getType();
        ArrayList<Contact> loadedContacts = new Gson().fromJson(contacts, listType);

        assertEquals(2, loadedContacts.size());

        Mockito.when(mockedRequest.getParameter("name")).thenReturn("Tres");

        servlet.doGet(mockedRequest, mockedResponse);

        Mockito.verify(mockedWriter, Mockito.times(2)).write(captor.capture());
        contacts = captor.getValue();

        listType = new TypeToken<ArrayList<Contact>>(){}.getType();
        loadedContacts = new Gson().fromJson(contacts, listType);

        assertEquals(3, loadedContacts.size());

        Mockito.when(mockedRequest.getParameter("name")).thenReturn("Pedro");

        servlet.doGet(mockedRequest, mockedResponse);

        Mockito.verify(mockedWriter, Mockito.times(3)).write(captor.capture());
        contacts = captor.getValue();

        listType = new TypeToken<ArrayList<Contact>>(){}.getType();
        loadedContacts = new Gson().fromJson(contacts, listType);

        assertEquals(0, loadedContacts.size());

        Mockito.when(mockedRequest.getParameter("name")).thenReturn(null);
        Mockito.when(mockedRequest.getParameter("cpf")).thenReturn("1");

        servlet.doGet(mockedRequest, mockedResponse);

        Mockito.verify(mockedWriter, Mockito.times(4)).write(captor.capture());
        contacts = captor.getValue();

        listType = new TypeToken<ArrayList<Contact>>(){}.getType();
        loadedContacts = new Gson().fromJson(contacts, listType);

        assertEquals(1, loadedContacts.size());

        Mockito.when(mockedRequest.getParameter("cpf")).thenReturn("1");
        Mockito.when(mockedRequest.getParameter("name")).thenReturn("Um");

        servlet.doGet(mockedRequest, mockedResponse);

        Mockito.verify(mockedWriter, Mockito.times(5)).write(captor.capture());
        contacts = captor.getValue();

        listType = new TypeToken<ArrayList<Contact>>(){}.getType();
        loadedContacts = new Gson().fromJson(contacts, listType);

        assertEquals(1, loadedContacts.size());

        Mockito.when(mockedRequest.getParameter("email")).thenReturn("um@aurum.com");
        Mockito.when(mockedRequest.getParameter("name")).thenReturn(null);
        Mockito.when(mockedRequest.getParameter("cpf")).thenReturn(null);

        servlet.doGet(mockedRequest, mockedResponse);

        Mockito.verify(mockedWriter, Mockito.times(6)).write(captor.capture());
        contacts = captor.getValue();

        listType = new TypeToken<ArrayList<Contact>>(){}.getType();
        loadedContacts = new Gson().fromJson(contacts, listType);

        assertEquals(2, loadedContacts.size());
    }

}
